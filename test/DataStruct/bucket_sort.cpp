/**
 * @file bucketsort.cpp
 * @author 水若可长 (SherlockZhang@aliyun.com)
 * @brief 桶排序，标记出现的次数,时间复杂O(M+N),浪费空间
 * @version 0.1
 * @date 2021-11-4
 *
 * @copyright Copyright (c) 2023
 *
 **/
#include <iostream>

void BucketSort(int sort[])
{
    int bucket[11] = {0};
    for (int i = 0; i < 10; i++)
    {
        bucket[sort[i]]++;
    }
    for (int i = 0; i < 11; i++)
    {
        for (; bucket[i] > 0; bucket[i]--)
        {
            std::cout << i << " ";
        }
    }
}
int main()
{
    int sort_array[10] = {2, 4, 1, 5, 0, 8, 6, 2, 10, 7};
    for (int i = 0; i < 10; i++)
    {
        std::cout << sort_array[i] << " ";
    }
    std::cout << std::endl;
    BucketSort(sort_array);
    return 0;
}