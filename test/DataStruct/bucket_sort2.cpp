/**
 * @file bucket_sort.cpp
 * @author 水若可长 (SherlockZhang@aliyun.com)
 * @brief 桶排序
 * @version 0.1
 * @date 2023-03-21
 *
 * @copyright Copyright (c) 2023
 *
 **/

#include <iostream>
using namespace std;

void buckerSort()
{
    int arr[10] = {9, 2, 3, 3, 1, 0, 8, 7, 6, 6};
    int arrsort[10] = {0};
    for (int i = 0; i < 10; i++)
    {
        arrsort[arr[i]]++;
    }
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < arrsort[i]; j++)
            cout << i << " ";
    }
    cout << endl;
}

int main()
{
    buckerSort();
    return 0;
}