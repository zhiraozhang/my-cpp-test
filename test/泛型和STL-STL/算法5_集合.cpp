#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

vector<int> test00(int val)
{
    vector<int> v;
    for (int i = 0; i < 10; i++)
    {
        v.push_back(i + val);
    }
    return v;
}
void test02()
{
    vector<int> v1 = test00(0);
    vector<int> v2 = test00(5);
    vector<int> v3;
    v3.resize(v1.size() + v2.size());
}
int main()
{
    return 0;
}