/**
 * @file container_string.cpp
 * @author SherlockZhang3 (SherlockZhang@aliyun.com)
 * @brief
 * @version 0.1
 * @date 2022-06-11
 *
 * @copyright Copyright (c) 2023
 *
 **/
#include <iostream>
#include <string>

void test00()
{
    // 字符串数组其实后面有一个/0结束符，所以虽然只存三个数但是要有4个空间
    char a[4] = {"aaa"};
    char b[4] = "bbb";
    // 也可以挨个赋值，这样就没有/0结束符了
    char c[3] = {'c', 'c', 'c'};
    std::cout << "字符串数组的两种赋值方法：" << a[0] << b[0] << c[0] << std::endl;
    std::cout << "可以通过首地址打印所有字符串：" << &a[0] << std::endl;
}

void test01()
{
    // 重载运算符+，==，!=,<,<=,>,>=,>>,<<
    std::string str1, str2, str3;
    str1 = {"Let's go!"};
    str2 = {"\aPikachu!\n"};
    str3 = str1 + str2;
    std::cout << "拼接字符串：" << str3;
}

int main()
{
    test00();
    test01();

    return 0;
}