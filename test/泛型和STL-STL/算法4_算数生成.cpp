
#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
using namespace std;

void test01()
{
    vector<int> v;
    for (int i = 0; i <= 100; i++)
    {
        v.push_back(i);
    }
    // accumulate第三个是起始累加值
    int total = accumulate(v.begin(), v.end(), 0);
    cout << total << endl;
}

void myprint(int val)
{
    cout << val << endl;
}

void test02()
{
    vector<int> v;
    v.resize(10);
    // 后期填充数据
    fill(v.begin(), v.end(), 100);
    for_each(v.begin(), v.end(), myprint);
    cout << endl;
}

int main()
{
    test01();
    test02();
    return 0;
}