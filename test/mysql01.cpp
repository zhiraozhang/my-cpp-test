/**
 * @file mysql01.cpp
 * @author SherlockZhang3 (SherlockZhang@aliyun.com)
 * @brief
 * @version 0.1
 * @date 2023-03-09
 *
 * @copyright Copyright (c) 2023
 *
 * mysql_init(MYSQL* mysql)
 * mysql_real_connect(
 * MYSQL *mysql,
 * const char *host,
 * const char *user,
 * )
 * void mysql_close(MYSQL* mysql)
 *
 **/
#include <iostream>
// #include </usr/include/mysql/mysql.h>
//  #include <WinSock.h>
#include <D:\\mysql-8.0.32\\include\\mysql.h>
using namespace std;

int main()
{
	MYSQL *mysql = new MYSQL;
	MYSQL_RES *res;
	MYSQL_FIELD *fd;  // 表头
	MYSQL_ROW column; // 行
	char query[150] = "select * from dept_manager;";
	//  int mysql_options(MYSQL * mysql, enum mysql_option option, const char *arg);
	//  中间分别是主机，用户名，密码，数据库名，端口号（可以写默认0或者3306等），可以先写成参数再传进去
	//  返回false则连接失败，返回true则连接成功
	if (mysql_init(mysql) == NULL) // mysql变量初始化
	{
		printf("error\n");
		exit(1); // 如果发生错误就退出程序
	}
	mysql_error(mysql);
	if (mysql_real_connect(mysql, "localhost", "waterz", "1qaz2wsx#EDC", "employees", 3306, 0, 0) == NULL)
		printf("Error connecting to database:%s", mysql_error(mysql));
	else
		cout << "connect success!" << endl;

	if (mysql_query(mysql, query)) // 执行SQL语句
	{
		printf("Query failed (%s)\n", mysql_error(mysql));
		return false;
	}
	else
	{
		printf("query success\n");
	}
	res = mysql_store_result(mysql);
	cout << "结果有" << mysql_field_count(mysql) << "列！" << endl;
	cout << "结果有" << mysql_affected_rows(mysql) << "行！" << endl;

	fd = mysql_fetch_fields(res);

	for (int i = 0; i < mysql_field_count(mysql); i++) // 打印字段
	{
		cout << fd[i].name << '\t';
	}
	cout << endl;
	while (column = mysql_fetch_row(res)) // 在已知字段数量情况下，获取并打印下一行
	{
		cout << column[0] << '\t' << column[1] << '\t' << column[2] << '\t' << column[3] << endl; // column是列数组
	}
	mysql_close(mysql);
	return 0;
}
