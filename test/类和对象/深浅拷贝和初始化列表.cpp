/**
 * @file 深浅拷贝和初始化列表.cpp
 * @author SherlockZhang3 (SherlockZhang@aliyun.com)
 * @brief 浅拷贝就是简单的复制拷贝，深拷贝重新在堆区重新创建空间，注意如果类中用到了指针，就需要深拷贝
 * @version 0.1
 * @date 2023-02-05
 *
 * @copyright Copyright (c) 2023
 *
 **/
#include <iostream>
using namespace std;
class Person
{
public:
	Person()
	{
		cout << "无参构造函数调用。" << endl;
	}
	Person(int age)
	{
		m_Age = age;
		cout << "有参构造函数调用。" << endl;
	}
	// 初始化列表构造
	Person(int a, int *b, int c) : m_Age(a), m_Height(b), m_Weight(c)
	{
		cout << "初始化列表构造函数调用。" << endl;
	}
	Person(int age, int height)
	{
		m_Age = age;
		m_Height = new int(height);
		cout << "有参构造函数02调用。" << endl;
	}
	Person(const Person &p)
	{
		m_Age = p.m_Age;
		m_Height = new int(*p.m_Height);
		cout << "拷贝构造函数调用。" << endl;
	}
	~Person()
	{
		// 如果没有置空操作，类析构之后 下面的s指针也能指到 之前的的数据
		// if (m_Height != NULL)
		//{
		//	delete m_Height;
		//	m_Height = NULL;
		// }
		cout << "析构函数调用。" << endl;
	}
	int m_Age;
	int *m_Height;
	int m_Weight;
};
void test(int *&s)
{
	Person p1(13, 170);
	cout << "p1年龄为" << p1.m_Age << "，p1身高为：" << *p1.m_Height << endl;
	Person p2(p1);
	cout << "p2年龄为" << p2.m_Age << "，p2身高为：" << *p2.m_Height << endl;
	cout << "p1的m_Height：" << p1.m_Height << "，p2的m_Height：" << p2.m_Height << endl;
	s = p2.m_Height;
}
int main()
{
	int *s = NULL;
	test(s);
	cout << "s指向的p2.Height为：" << *s << endl;

	// 初始化列表方式构造
	int *height = new int(180);
	Person p(25, height, 150);
	cout << p.m_Age << " " << *p.m_Height << " " << p.m_Weight << endl;
	delete height;
	return 0;
}